import { Injectable } from '@angular/core';
import{HttpClient,HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { RESOURCE_CACHE_PROVIDER } from '@angular/platform-browser-dynamic';
@Injectable()
export class AvatarService {
  public headers = new Headers();
  public urlpersonas:string ="http://localhost:8080/message/avatar";
   

  constructor(private httpClient: HttpClient,public _http: Http ) { 
    this.headers.append("Content-Type","application/json");

  }

  
  protected obtenerHeaders(): Headers {
    let headers = new Headers();
 //   let sesion = SesionService.obtenerElementos();

    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    headers.append('Acces-Control-Allow-Headers','Content-Type');

    return headers;
  }

  obtenerPeliculas(url:string){
    
    console.log(url)
    console.log( this.httpClient.get(url))
    return this.httpClient.get(url)
  }

    obtenerPersonas(url:string){
      let queryParams = new URLSearchParams()
 //  queryParams.append();
let headers= this.obtenerHeaders();
    headers.append('requestUrl', url);
    let options = new RequestOptions({
      headers:headers, 
      search:queryParams
    });
    
    console.log(url)
    return this._http.get('http://localhost:8080/message/character-url',options).map((res:Response)=>res.json());
    
    }

    obtenerDetalle(filmName:string){
      let queryParams= new URLSearchParams()
      let headers=this.obtenerHeaders();
      headers.append('filmName',filmName);
      let options = new RequestOptions({
       headers:headers,
       search:queryParams
})
      console.log(filmName)
      return this._http.get('http://localhost:8080/message/listarxFilm',options).map((res:Response)=> res.json());
    }

  
    


 


}
  