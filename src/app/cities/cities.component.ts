import { Component, OnInit } from '@angular/core';
import { AvatarService } from 'app/avatar.service';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {
resultas:any;
  results:any;
  nombrePersonajes :string []=[];
  persona:any;
  detalleDePeli: string []=[];
  casting:any
  constructor(private loginService:AvatarService) { 
    
    this.loginService.obtenerPeliculas('http://localhost:8080/message/avatar').subscribe((res:any)=>{
this.results=res.results
console.log(this.results)
      console.log(res);
    })
  }
  director:string;
   producer:string;
   episode_id:any;
   release_date:string;
  public detalle(city){
    
    this.loginService.obtenerDetalle(city.episode_id).subscribe((res:any)=>{
      console.log(city.episode_id)
      this.detalleDePeli.push(res.opening_crawl);
      this.detalleDePeli.push(res.director);
      console.log(res.director)
      
      this.director=res.director;
      this.producer=res.producer;
      this.episode_id=res.episode_id;
      this.release_date=res.release_date;
      this.detalleDePeli.push(res.producer);
  
      this.detalleDePeli.push(res.episode_id);
      this.detalleDePeli.push(res.release_date);
      console.log(this.detalleDePeli);
      this.casting=this.detalleDePeli;
      console.log(this.casting)
    })
       for (let x of city.characters) {
      let nombres: string =x;
    
       this.loginService.obtenerPersonas(nombres).subscribe((res:any)=>{
    //console.log(nombres)
        
  
  //this.results=res.name
   this.nombrePersonajes.push(res.name);
   this.persona=this.nombrePersonajes;
    });

    
                                }
   
  }
     
   
   public borrar(){
     this.detalleDePeli=[];
this.casting=[];
this.nombrePersonajes=[];
   }
  ngOnInit() {

    // this.obtenerAvatar();
  }
}
